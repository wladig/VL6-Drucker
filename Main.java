package VL6.Drucker;

public class Main {

    public static void main(String[] args) 
    {
        Printer p1 = new Printer(500, 4500);
        
        p1.print(1, 199, false);
        p1.printStats();
        p1.print(200, 300, false);
        p1.printStats();
        p1.print(900, 2, false);
        p1.printStats();
        p1.print(1, 1000, false);
        p1.printStats();
        
    }

}
