package VL6.Drucker;

public class Printer 
{
    private int pagesMax;
    private int pagesAvailable;
    private int tonerMax;
    private int tonerAvailable;
    
    private static int statsPagesPrinted;
    private static int statsTonerUsed;
    private static int statsPagesRenewed;
    private static int statsTonerRenewed;
    
    public Printer(int pagesMax, int tonerMax)
    {
        this.pagesMax = pagesMax;
        this.pagesAvailable = pagesMax;
        this.tonerMax = tonerMax;
        this.tonerAvailable = tonerMax;
    }
    
    public void print(int copies, int pagesToPrint, boolean extendedLog)
    {
        for (int j = 1; j <= copies; j++) 
        {
            for (int i = 1; i <= pagesToPrint; i++) 
            {
                if( pagesAvailable>0 && tonerAvailable>0 )
                {
                    pagesAvailable--;
                    tonerAvailable--;
                    if(extendedLog)
                    {
                        System.out.println("Seite " + i + " von " + pagesToPrint + ": wurde ausgedruckt.");
                    }
                }
                else
                {
                    if( pagesAvailable == 0 )
                    {
                        renewPages(i, pagesToPrint);
                    }
                    if( tonerAvailable == 0 )
                    {
                        renewToner(i, pagesToPrint);
                    }
                }
                
                statsPagesPrinted++;
                statsTonerUsed++;
            }
        }
    }
    
    private void renewPages(int curPage, int pagesToPrint)
    {
        pagesAvailable = pagesMax;
        System.out.println("Seite " + curPage + " von " + pagesToPrint + ": Papier wurde nachgefüllt.");
        pagesAvailable--;
        statsPagesRenewed++;
    }
    
    private void renewToner(int curPage, int pagesToPrint)
    {
        tonerAvailable = tonerMax;
        System.out.println("Seite " + curPage + " von " + pagesToPrint + ": Toner wurde nachgefüllt.");
        tonerAvailable--;
        statsTonerRenewed++;
    }
    
    public void printStats()
    {
        System.out.println("***************************************************");
        System.out.println("Total pages renewed: " + statsPagesRenewed);
        System.out.println("Total toner renewed: " + statsTonerRenewed);
        
        System.out.println("Total printed Pages: " + statsPagesPrinted);
        System.out.println("Total toner used for pages: " + statsTonerUsed);
        System.out.println("***************************************************");        
    }
    
}
